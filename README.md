# **Digital and Soft Skills for Interdisciplinary Project Development - challenge**
## **Download a Python Repo and Run it Using Anaconda**
Dominik Leitner
## **geemap**


### **introduction**
**geemap** is a Python package for interactive mapping with [Google Earth Engine](https://earthengine.google.com/) (GEE), which is a cloud computing platform with a [multi-petabyte catalog](https://developers.google.com/earth-engine/datasets/) of satellite imagery and geospatial datasets. During the past few years, GEE has become very popular in the geospatial community and it has empowered numerous environmental applications at local, regional, and global scales. GEE provides both JavaScript and Python APIs for making computational requests to the Earth Engine servers.

credits: [Qiusheng Wu](https://github.com/giswqs)
original Github source [link](https://github.com/giswqs/geemap#examples)

a youtube tutorial to follow along by [burdGIS](https://www.youtube.com/watch?v=H6MpxlMbjeY&list=WL&index=27&ab_channel=burdGIS)
-- --

### **software setup for repository**

**I. Anaconda - virtual environment manager**
provide an environment specific to a project
&rarr; section the project of from the rest of the machine (seperates the dependencies of different projects) &rarr; isolated environment
download Anaconda [here](https://www.anaconda.com/products/individual)

1.) create a new anaconda virtual environment called:
```
geemap_demo
```
![01_create_environment.png](./media/01_create_environment.png)
&rarr; Python version should be **3.7**
**note:** the installation steps were done under Python 3.7 with packages/dependencies suited to that version
for updated installation steps under a newer Python check [here](https://github.com/giswqs/geemap/blob/master/README.rst#installation) **!**
<br>
2.) install the following packages using the command terminal:
![02_install_packages_terminal.png](./media/02_install_packages_terminal.png)
```
conda install geopandas
```
```
conda install mamba -c conda-forge
```
```
mamba install geemap xarray_leaflet -c conda-forge
```
![03_install_packages.png](./media/03_install_packages.png)
be patient and wait for the installations to be successfully executed
-- --
<br>

**II. Jupyter notebook - web-based interactive computing platform**

1.) install Jupyter notebook in anaconda:
![04_jupyter_notebook.png](./media/04_jupyter_notebook.png)
2.) launch Jupyter notebook via the anaconda environment:
![05_launch_jupyter_notebook.png](./media/05_launch_jupyter_notebook.png)
&rarr; opens browser window

3.) add a new **kernel** via the terminal:
```
ipython kernel install --name "geemap_demo" --user
```
![06_create_kernel.png](./media/06_create_kernel.png)
&rarr; refresh browser tab

4.) open geemap_demo notebook

5.) check if geemap was installed successfully:
```
import geemap
```
![07_check_geemap.png](./media/07_check_geemap.png)
-- --
<br>

**III. Google Earth Engine**
In order to create .gifs from landsat images you have to create a Google Earth Engine account beforehand:
[create account here](https://earthengine.google.com/) - sign up
-- --
<br>

**IV. create a timelapse .gif from Google Earth landsat images**
geemap provides you with a lot of different possibilities to create interesting .gifs from satelite images:
[find more examples here](https://github.com/giswqs/geemap/tree/master/examples/notebooks)

For example purposes we are going over one possibility in the following and run it in our own jupyter notebook session:
1.) download 39_timelapse.ipynb from [here](https://github.com/giswqs/geemap/blob/master/examples/notebooks/39_timelapse.ipynb) (in **raw** format):
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1) right click
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2) save page as
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3) create new folder in downloads called:
```
geemapNotebooks
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4) save 39_timelapse.ipynb in geemapNotebooks

2.) open a terminal in anaconda (geemap_demo environment)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1) redirect to the **geemapNotebooks** directory
![08_change_directory.png](./media/08_change_directory.png)

3.) type in the command terminal to open jupyter notebook:
```
jupyter notebook
```
![09_start_jupyter_notebook.png](./media/09_start_jupyter_notebook.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rarr; the file opens in the browser
4.) change kernel from Python 3 to geemap_demo
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; kernel &rarr; change kernel
![10_change_kernel.png](./media/10_change_kernel.png)

5.) run notebook/cells
![11_run_cells.png](./media/11_run_cells.png)

&rarr; explore and play around the different options given in the user interface
possible parameters are:
- location
- RGB combo
- frames per second
- start to end year
- etc.

create timelapse &rarr; file will be saved to the **downloads** folder
-- --
### Aral Sea
lake effected by man-made catastrophy and decreasing in size ever since
[article by earthobservatory](https://earthobservatory.nasa.gov/world-of-change/AralSea)

![12_Aralsee.gif](./media/12_Aralsee.gif)
